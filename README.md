# Thư Viện xanh

Thư Viện xanh chia sẻ kiến trúc xanh, không gian xanh, cây xanh, sân vườn hướng tới cuộc sống thân thiện, bền vững với thiên nhiên.
- Địa chỉ: 124 Minh Khai, Hai Bà Trưng, Hà Nội
- SDT: 0985254376

https://thuvienxanh.com/

https://flipboard.com/@ThVinxanh

https://www.twitch.tv/thuvienxanh/about

https://about.me/thuvienxanh/

Cây cảnh công sở ngày càng đc chăm lo & chú trọng hơn vì nó có không ít tính năng tích cực cho con người. Có khá nhiều loại cây cảnh đc sử dụng để trang trí vô cùng đa dạng và mê hoặc, có thể treo tường và đặt cây nhiều chỗ đứng như trước cửa, trung tâm ở trong nhà, một bên bàn thao tác.

Cây cảnh văn phòng sẽ khiến nơi thao tác thêm xanh đẹp, thật sạch sẽ, tạo thêm khoảng không thoáng đảng. Cung cấp hàm lượng oxy đáng kể để hoạt động hô hấp của con người ra mắt thuận tiện hơn đồng thời còn thải ra hơi nước tạo xúc cảm rất mẻ nên với các ngày nóng ran nóng bức khi ngay gần bên cây sẽ rất lạnh mát và thoải mái. Căn phòng sẽ trở nên tươi tắn, tiến bộ hơn với khá nhiều kiêu cây cảnh đa chủng loại, các lúc thao tác làm việc mệt mỏi người làm có thể thư giản bằng phương pháp ngắm ngía, quan tâm cho những chậu cây cảnh giúp thư thái niềm tin và lại giúp dược cây phát triển xuất sắc hơn
